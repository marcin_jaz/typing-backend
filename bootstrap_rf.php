<?php defined('SYSPATH') or die('No direct script access.');

//-- Environment setup --------------------------------------------------------
//$_SERVER['PATH_INFO'] = $_SERVER['REQUEST_URI'];
/**
 * Set the default time zone.
 *
 * @see  http://kohanaframework.org/guide/using.configuration
 * @see  http://php.net/timezones
 */
date_default_timezone_set('Europe/Warsaw');

/**
 * Set the default locale.
 *
 * @see  http://kohanaframework.org/guide/using.configuration
 * @see  http://php.net/setlocale
 */
setlocale(LC_ALL, 'en_US.utf-8');

/**
 * Enable the Kohana auto-loader.
 *
 * @see  http://kohanaframework.org/guide/using.autoloading
 * @see  http://php.net/spl_autoload_register
 */
spl_autoload_register(array('Kohana', 'auto_load'));

/**
 * Enable the Kohana auto-loader for unserialization.
 *
 * @see  http://php.net/spl_autoload_call
 * @see  http://php.net/manual/var.configuration.php#unserialize-callback-func
 */
ini_set('unserialize_callback_func', 'spl_autoload_call');

//-- Configuration and initialization -----------------------------------------

/**
 * Initialize Kohana, setting the default options.
 *
 * The following options are available:
 *
 * - string   base_url    path, and optionally domain, of your application   NULL
 * - string   index_file  name of your index file, usually "index.php"       index.php
 * - string   charset     internal character set used for input and output   utf-8
 * - string   cache_dir   set the internal cache directory                   APPPATH/cache
 * - boolean  errors      enable or disable error handling                   TRUE
 * - boolean  profile     enable or disable internal profiling               TRUE
 * - boolean  caching     enable or disable internal caching                 FALSE
 */
Kohana::init(array(
	'base_url'   => 'http://www.mojeligi.eu',
	'caching' => TRUE,
	'errors' => TRUE,
'cache_dir'=> APPPATH.'/cache',
));


//if (Kohana::$errors)
//{
//    set_exception_handler(array('Kohana_Exception', 'handler'));
//}
/**
 * Attach the file write to logging. Multiple writers are supported.
 */
Kohana::$log->attach(new Kohana_Log_File(APPPATH.'logs'));

/**
 * Attach a file reader to config. Multiple readers are supported.
 */
Kohana::$config->attach(new Kohana_Config_File);

/**
 * Enable modules. Modules are referenced by a relative or absolute path.
 */
Kohana::modules(array(
// 'auth'       => MODPATH.'auth',       // Basic authentication
//'cache'      => MODPATH.'cache',      // Caching with multiple backends
// 'codebench'  => MODPATH.'codebench',  // Benchmarking tool
	 'database'   => MODPATH.'database',   // Database access
	 'calendar'   => MODPATH.'calendar',   // Calendar 
// 'image'      => MODPATH.'image',      // Image manipulation
// 'orm'        => MODPATH.'orm',        // Object Relationship Mapping
// 'oauth'      => MODPATH.'oauth',      // OAuth authentication
// 'pagination' => MODPATH.'pagination', // Paging of results
// 'unittest'   => MODPATH.'unittest',   // Unit testing
// 'userguide'  => MODPATH.'userguide',  // User guide and API documentation
));


/**
 * Set the routes. Each route must have a minimum of a name, a URI and a set of
 * defaults for the URI.
 */
#
Route::set('admin', 'admin(/<controller>(/<action>(/<id>)))')
->defaults(array(
	'directory' => 'admin',
	'controller' => 'panel',
	'action' => 'index',
));

Route::set('ajax', 'ajax(/<controller>(/<action>(/<id>)))', array('id' => '.*'))
->defaults(array(
	'directory' => 'ajax',
	'controller' => 'team',
	'action' => 'index',
));




Route::set('admin2', 'admin(/<controller>(/<action>(/<id>)))', array('id' => '.*'))
->defaults(array(
	'directory' => 'admin',
	'controller' => 'panel',
	'action' => 'index',
));


//pozwala na usuniecie index
Route::set('defaultlittle', '<controller>(/<id>)', array('id' => '([0-9]++).*'))//'id' => '.*',
->defaults(array(
		'controller' => 'team',
		'action'     => 'index',
));


//usuniecie index, ale uwzglednienie parametru co nei jest cyfrą
Route::set('defaulstats', '<controller>(/<country>(/<id2>))', array('country' => '[A-Z].*', 'id2' => '([0-9]++).*'))//'id' => '.*',
->defaults(array(
		'controller' => 'leagues',
		'action'     => 'index',
));


//w przypadku gdy nie jest akcja index, jest potrzebny
Route::set('default', '(<controller>(/<action>(/<id>)))', array('id' => '.*'))
->defaults(array(
		'controller' => 'main',
		'action'     => 'index',
));

//TODO to wlasciwie nie jest wykorzystane
Route::set('catch_all', 'errorhandler/<action>(/<message>)', array('action' => '[0-9]++', 'message' => '.+'))
->defaults(array(
    'controller' => 'errorhandler'
));

//if ( ! defined('SUPPRESS_REQUEST'))
//{
define("IN_PRODUCTION",false);
/**
 * Execute the main request. A source of the URI can be passed, eg: $_SERVER['PATH_INFO'].
 * If no source is specified, the URI will be automatically detected.
 */
try {
	echo Request::instance()
	->execute()
	->send_headers()
	->response;

}catch (Exception $e) {
	if ( ! IN_PRODUCTION) {
		// Just re-throw the exception
		throw $e;
	}

	//&& $this->request == Request::instance()
	if(!Request::$is_ajax )
	{
		$request = Request::factory('errorhandler/404');
		$request->status=404;//jebany status nie dziala
		$request->send_headers();
		echo $request->execute();
//		header("Location: http://www.mojeligi.pl/errorhandler/404");
//		exit;
	}
	
}


//$request = Request::instance();
////
//$parts = explode('/',$request->uri());
//$count = count($parts);
//if($count>0 && $parts[0]=='ajax')$parts[0]=$parts[1];
////// If page cache is loaded read the request variables from the cache
//if (Session::instance()->get('user_id',0)==0 && ($count==0 || ( $count>0 && ( $parts[0]=='league' || $parts[0]=='leagues' || $parts[0]=='interleagues' ||$parts[0]=='stages' ))) &&
//$page = Page::load($_SERVER['REQUEST_URI']))
//{
//	//	echo 'afsdsfasdf';
//	if (Expires::get())//if modified
//	{
//		$request->status    = 304;
//		$request->response  = '';
//	}
//	else
//	{
//		$request->status    = $page['status'];
//		$request->response  = $page['response'];
//	}
//
//	$request->headers   = $page['headers'];
//}
//else
//{
//	// Attempt to execute the response
//
//	try {
//		$request->send_headers();//TODO to dodalem
//		$request->response = (string) $request->execute()->response;
//
//	}catch (Exception $e) {
//		if ( ! IN_PRODUCTION) {
//			// Just re-throw the exception
//			throw $e;
//		}
//		$request = Request::factory('errorhandler/404');
//		echo $request->execute();
//	}
//}
//echo $request->response;
//}
