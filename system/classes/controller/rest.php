<?php defined('SYSPATH') or die('No direct script access.');

abstract class Controller_REST extends Kohana_Controller_REST {

	protected function retrieveJsonPost(){
		return json_decode ( file_get_contents("php://input") );
	}
}
