SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

ALTER SCHEMA `typing2`  DEFAULT CHARACTER SET utf8  DEFAULT COLLATE utf8_general_ci ;

ALTER TABLE `users` 
CHARACTER SET = utf8 ,
ADD INDEX `loginProvider` (`provider` ASC, `provider_id` ASC),
DROP INDEX `emailprovicer` ,
DROP INDEX `forsearch` ;

ALTER TABLE `users_typing_defaults` 
CHARACTER SET = utf8 ,
CHANGE COLUMN `use_focus` `use_focus` TINYINT(1) NOT NULL DEFAULT true ;

ALTER TABLE `competitions` 
CHARACTER SET = utf8 ,
ADD COLUMN `group_id` INT(10) UNSIGNED NULL DEFAULT NULL AFTER `private_key`,
ADD INDEX `fk_competitions_groups1_idx` (`group_id` ASC);

ALTER TABLE `approaches` 
CHARACTER SET = utf8 ,
DROP INDEX `dateDesc` ,
ADD INDEX `dateDesc` (`date` DESC);

ALTER TABLE `competitor_approaches` 
CHARACTER SET = utf8 ;

CREATE TABLE IF NOT EXISTS `groups` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `lang` VARCHAR(2) NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  `started` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `private_key` VARCHAR(45) NOT NULL,
  `created_by` INT(10) UNSIGNED NOT NULL,
  `show_others` TINYINT(1) NOT NULL DEFAULT true,
  `archived_time` TIMESTAMP NULL DEFAULT NULL,
  INDEX `fk_groups_users1_idx` (`created_by` ASC),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_groups_users1`
    FOREIGN KEY (`created_by`)
    REFERENCES `users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

CREATE TABLE IF NOT EXISTS `group_users` (
  `user_id` INT(10) UNSIGNED NOT NULL,
  `group_id` INT(10) UNSIGNED NOT NULL,
  `joined` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  INDEX `fk_group_users_users1_idx` (`user_id` ASC),
  INDEX `fk_group_users_groups1_idx` (`group_id` ASC),
  UNIQUE INDEX `once` (`user_id` ASC, `group_id` ASC),
  CONSTRAINT `fk_group_users_users1`
    FOREIGN KEY (`user_id`)
    REFERENCES `users` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_group_users_groups1`
    FOREIGN KEY (`group_id`)
    REFERENCES `groups` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

ALTER TABLE `competitions` 
ADD CONSTRAINT `fk_competitions_groups1`
  FOREIGN KEY (`group_id`)
  REFERENCES `groups` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
