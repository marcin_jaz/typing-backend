Alter table competitions add column end_time datetime after start_time;
update competitions set end_time=DATE_ADD(start_time,INTERVAL 7 DAY);
ALTER TABLE competitions MODIFY end_time datetime NOT NULL;