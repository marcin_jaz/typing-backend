SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `je117468_typing` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `je117468_typing` ;

-- -----------------------------------------------------
-- Table `je117468_typing`.`users`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `je117468_typing`.`users` ;

CREATE TABLE IF NOT EXISTS `je117468_typing`.`users` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `nick` VARCHAR(45) NOT NULL,
  `password` VARCHAR(45) NULL,
  `email` VARCHAR(45) NOT NULL,
  `site_lang` VARCHAR(45) NOT NULL DEFAULT 'pl',
  `joined` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `provider_id` VARCHAR(45) NULL,
  `provider` VARCHAR(15) NULL,
  `picture` VARCHAR(255) NULL,
  `last_changed_nick` DATETIME NULL,
  `gender` VARCHAR(15) NULL COMMENT 'could be enum but not sure if it is always known, so male, female and eg unknown?',
  `ranking` INT UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `uniqueProviderEmail` (`provider` ASC, `email` ASC),
  INDEX `forsearch` (`email` ASC, `nick` ASC, `password` ASC),
  INDEX `emailprovicer` (`email` ASC, `provider` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `je117468_typing`.`users_typing_defaults`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `je117468_typing`.`users_typing_defaults` ;

CREATE TABLE IF NOT EXISTS `je117468_typing`.`users_typing_defaults` (
  `user_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `lang` VARCHAR(2) NOT NULL,
  `write_minutes` TINYINT UNSIGNED NOT NULL DEFAULT 1,
  `learn_minutes` TINYINT UNSIGNED NOT NULL DEFAULT 3,
  `write_config` SMALLINT UNSIGNED NOT NULL DEFAULT 1000,
  `learn_config` SMALLINT UNSIGNED NOT NULL DEFAULT 5 COMMENT 'number of last group included',
  `updated` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  INDEX `fk_user_config_User_idx` (`user_id` ASC),
  PRIMARY KEY (`user_id`, `lang`),
  CONSTRAINT `fk_user_config_User`
    FOREIGN KEY (`user_id`)
    REFERENCES `je117468_typing`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `je117468_typing`.`competitions`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `je117468_typing`.`competitions` ;

CREATE TABLE IF NOT EXISTS `je117468_typing`.`competitions` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `start_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `typing_minutes` TINYINT UNSIGNED NOT NULL,
  `typing_words` SMALLINT UNSIGNED NOT NULL,
  `lang` VARCHAR(45) NOT NULL,
  `text` BLOB NOT NULL,
  `founder_user_id` INT UNSIGNED NOT NULL,
  `private_key` VARCHAR(45) NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_competitions_users1_idx` (`founder_user_id` ASC),
  INDEX `private_key` (`id` ASC, `private_key` ASC),
  CONSTRAINT `fk_competitions_users1`
    FOREIGN KEY (`founder_user_id`)
    REFERENCES `je117468_typing`.`users` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `je117468_typing`.`approaches`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `je117468_typing`.`approaches` ;

CREATE TABLE IF NOT EXISTS `je117468_typing`.`approaches` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `wpm` SMALLINT UNSIGNED NOT NULL,
  `accuracy` FLOAT UNSIGNED NOT NULL,
  `keystrokes` INT UNSIGNED NOT NULL,
  `typing_minutes` TINYINT UNSIGNED NOT NULL,
  `lang` VARCHAR(45) NOT NULL,
  `config` SMALLINT UNSIGNED NOT NULL,
  `user_id` INT UNSIGNED NOT NULL,
  `type` ENUM('write','learn','competition') NOT NULL,
  `result` FLOAT UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  INDEX `fk_approaches_users1_idx` (`user_id` ASC),
  INDEX `dateDesc` (`date` DESC),
  INDEX `result_in_type_desc` (`type` ASC, `result` ASC),
  CONSTRAINT `fk_approaches_users1`
    FOREIGN KEY (`user_id`)
    REFERENCES `je117468_typing`.`users` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `je117468_typing`.`competitor_approaches`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `je117468_typing`.`competitor_approaches` ;

CREATE TABLE IF NOT EXISTS `je117468_typing`.`competitor_approaches` (
  `approach_id` INT UNSIGNED NOT NULL,
  `competition_id` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`approach_id`, `competition_id`),
  INDEX `fk_approaches_has_competitor_approaches1_idx` (`approach_id` ASC),
  INDEX `fk_competitor_approaches_competitions1_idx` (`competition_id` ASC),
  CONSTRAINT `fk_approaches_has_competitor_approaches1`
    FOREIGN KEY (`approach_id`)
    REFERENCES `je117468_typing`.`approaches` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_competitor_approaches_competitions1`
    FOREIGN KEY (`competition_id`)
    REFERENCES `je117468_typing`.`competitions` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `je117468_typing`.`hash_operations`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `je117468_typing`.`hash_operations` ;

CREATE TABLE IF NOT EXISTS `je117468_typing`.`hash_operations` (
  `email` VARCHAR(45) NOT NULL,
  `password` VARCHAR(45) NOT NULL,
  `date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `hash` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`email`),
  INDEX `forsearch` (`email` ASC, `hash` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `je117468_typing`.`logs`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `je117468_typing`.`logs` ;

CREATE TABLE IF NOT EXISTS `je117468_typing`.`logs` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `url` VARCHAR(255) NULL,
  `message` TEXT NULL,
  `stacktrace` TEXT NULL,
  `cause` VARCHAR(1024) NULL,
  `user` TEXT NULL,
  `when` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC))
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
