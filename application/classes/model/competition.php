<?php
defined('SYSPATH') or die ('No direct script access.');

class Model_Competition extends Kohana_Model
{

    const COMPETITION_LENGTH_IN_DAYS = 15;

    public function getAll()
    {
        $q = 'select c.id, c.end_time, c.typing_minutes, c.typing_words, c.lang, ROUND(max(a.result),2) as best_result,
				count(ca.competition_id) as approaches, count(distinct a.user_id) as users
				from competitions c
				left join competitor_approaches ca on c.id=ca.competition_id
				left join approaches a on ca.approach_id=a.id
				where private_key="" and c.end_time > CURRENT_TIMESTAMP
				group by c.id
				having approaches>0 
				order by c.end_time asc;';
        return $this->_db->query(Database::SELECT, $q, true)->as_array();
    }

    public function getUserHistory($userId)
    {
        $q = 'select c.id, c.private_key, c.end_time, c.typing_minutes, c.typing_words, c.lang, ROUND(max(a.result),2) as best_result,
				count(ca.competition_id) as approaches, 
				c.end_time > CURRENT_TIMESTAMP as is_active,
				(
					select count(distinct a1.user_id)+1
					FROM approaches a1
					JOIN competitor_approaches ca1 on ca1.approach_id = a1.id
					where ca1.competition_id=ca.competition_id and a1.result>max(a.result)
				) as rank
				from competitions c
				left join competitor_approaches ca on c.id=ca.competition_id
				left join approaches a on ca.approach_id=a.id
				where a.user_id=' . $userId . ' 
				group by c.id
				order by is_active desc,
				CASE is_active WHEN true THEN c.END_TIME END asc,
				CASE is_active WHEN false THEN c.end_time END desc
				limit 20;';
//        c.end_time desc
        return $this->_db->query(Database::SELECT, $q, true)->as_array();
    }

    public function getUserHistoryFirstThreePositions($userId)
    {
        $q = 'SELECT rank, count(*) as times from
				 (SELECT  				
					(
						SELECT count(distinct a1.user_id)
						FROM approaches a1
						JOIN competitor_approaches ca1 on ca1.approach_id = a1.id
						where ca1.competition_id=ca.competition_id  and a1.result>max(a.result)
					) AS rank
					FROM  competitor_approaches ca 
					JOIN approaches a on ca.approach_id=a.id
					JOIN competitions c on c.id = ca.competition_id
					WHERE a.user_id=' . $userId . ' and c.private_key=""
					GROUP by ca.competition_id
	 				HAVING rank<4
				) as ranking group by rank
			  order by rank asc;';
        $result = $this->_db->query(Database::SELECT, $q, true)->as_array();

        $array[0] = 0;
        $array[1] = 0;
        $array[2] = 0;
        foreach ($result as $v) {
            $array[$v->rank] = $v->times;
        }
        return $array;
    }

    public function getPositionsOfCompetition($id)
    {
        $q = 'select ROUND(max(a.result),2) as best_result, u.id, u.nick, u.picture
				from competitor_approaches ca
				join approaches a on ca.approach_id=a.id
				join users u on u.id = a.user_id
				where ca.competition_id=' . $id . '
				group by a.user_id
				order by best_result desc, a.accuracy desc;';
        return $this->_db->query(Database::SELECT, $q, true)->as_array();
    }

    public function getCompetitionsForGroup($groupId)
    {
        $q = 'select c.id, c.typing_words, c.typing_minutes, c.end_time, c.private_key,
				c.end_time > CURRENT_TIMESTAMP as is_active
				from competitions c
				where c.group_id=' . $groupId . '
				order by c.end_time desc;';
        return $this->_db->query(Database::SELECT, $q, true)->as_array();
    }

    public function getPositionsOfCompetitionsForGroup($groupId, $competitionsIds)
    {
        $columnsSql = '';
        $queriesSql = '';
        for ($i = 0; $i < count($competitionsIds); $i++) {
            $columnsSql .= ', IFNULL(r' . $i . '.best,0) as result' . $i;
            $queriesSql .= ' left join (
				select ROUND(max(a.result),2) as best, a.user_id
                from competitor_approaches ca 
				left join approaches a on ca.approach_id=a.id 
				where ca.competition_id=' . $competitionsIds[$i] . '
				group by a.user_id
				    ) as r' . $i . '
				on u.id=r' . $i . '.user_id';
        }
        $q = 'select u.id, u.nick, u.picture' . $columnsSql . '
				from
			(
				select u.id, u.nick, u.picture
				from group_users gu
				join users u on u.id=gu.user_id
				where gu.group_id= ' . $groupId . '
			) as u' . $queriesSql;
        // print_r($q);
        // print_r ($columnsSql);
        // exit()
        return $this->_db->query(Database::SELECT, $q, true)->as_array();
    }

    public function getCompetitionLastCreatedByUser($founder)
    {
        $q = 'select id, TIMESTAMPDIFF(MINUTE, start_time, NOW()) as minutes_since_created, private_key
				from competitions
				where founder_user_id = ' . $founder . ' and group_id is null
				order by start_time desc
				limit 1;';
        return $this->_db->query(Database::SELECT, $q, true)->current();
    }

    public function save($competition)
    {
        $q = 'INSERT INTO competitions
		(end_time, typing_minutes, typing_words, private_key, lang, founder_user_id, text, group_id)
		VALUES (DATE_ADD(CURRENT_TIMESTAMP,INTERVAL ' . Model_Competition::COMPETITION_LENGTH_IN_DAYS . ' DAY),"' .
            $competition->typing_minutes . '","' . $competition->typing_words . '","' . $competition->private_key .
            '","' . $competition->lang . '",' . $competition->founder_user_id . ',"' . $competition->text . '",' .
            $competition->group_id . ' ) ';
        return $this->_db->query(Database::INSERT, $q, false);
    }

    public function get($id)
    {
        $q = 'select *
		from competitions
		where id=' . $id . '
		limit 1;';
        return $this->_db->query(Database::SELECT, $q, true)->current();
    }

    public function getIfActive($id, $key)
    {
        if (!is_numeric($id)) {
            return null;
        }
        if ("undefined" == $key) {
            $key = "";
        }
        $q = 'select *
		from competitions
		where id=' . $id . '
		and private_key="' . $key . '" 
		and end_time > CURRENT_TIMESTAMP
		limit 1;';
        // for public key will be always empty
        return $this->_db->query(Database::SELECT, $q, true)->current();
    }
}
