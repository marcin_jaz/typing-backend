<?php
defined ( 'SYSPATH' ) or die ( 'No direct script access.' );

class Model_User extends Kohana_Model {

	public function get($id) {
		$q = 'select id, nick, site_lang, joined, picture, TIMESTAMPDIFF(DAY,NOW(),last_changed_nick) as can_change_nick
		from users
		where id=' . $id . ' 
		limit 1;';
		return $this->_db->query ( Database::SELECT, $q, true )->current ();
	}

	public function changeNick($id, $nick) {
		$q = 'update users set nick="' . $nick . '", last_changed_nick=NOW()
		where id=' . $id . '
		limit 1;';
		return $this->_db->query ( Database::UPDATE, $q, false );
	}

	public function changePicture($id, $url) {
		$q = 'update users set picture="' . $url . '"
		where id=' . $id . '
		limit 1;';
		return $this->_db->query ( Database::UPDATE, $q, false );
	}

	public function changeSiteLang($id, $siteLang) {
		$q = 'update users set site_lang="' . $siteLang . '"
		where id=' . $id . '
		limit 1;';
		return $this->_db->query ( Database::UPDATE, $q, false );
	}

	public function changeProviderId($id, $providerId) {
		$q = 'update users set provider_id="' . $providerId . '"
		where id=' . $id . '
		limit 1;';
		return $this->_db->query ( Database::UPDATE, $q, false );
	}

	public function insert($user) {
		$this->ajustPictureSize ( $user );
		
		if(! isset ( $user->gender )) {
			$user->gender = 'unknown';
		}
		
		$q = 'INSERT ignore INTO users
		(nick, email, site_lang, provider, provider_id, picture, gender)
		VALUES ("' . $user->name . '","' . $user->email . '","' . $user->site_lang . '","' . $user->provider . '","' . $user->id . '","' . $user->picture . '","' . $user->gender . '" ) ';
		return $this->_db->query ( Database::INSERT, $q, false );
	}

	private function ajustPictureSize($user) {
		if($user->provider == 'google') {
			$user->picture .= '?sz=100';
		} else if($user->provider == 'facebook') {
			$user->picture .= '?type=normal';
		}
	}

	public function saveConfig($config) {
		return $this->doSaveConfig ( $config, 'ON DUPLICATE KEY UPDATE write_minutes=values(write_minutes),learn_minutes=values(learn_minutes),write_config=values(write_config),learn_config=values(learn_config), use_focus=values(use_focus), updated=CURRENT_TIMESTAMP;' );
	}

	public function saveDefaultForLangOrMarkLatest($config) {
		return $this->doSaveConfig ( $config, 'ON DUPLICATE KEY UPDATE updated=CURRENT_TIMESTAMP;' );
	}

	private function doSaveConfig($config, $updatePart) {
		$config->use_focus = ( string ) $config->use_focus;
		$q = 'INSERT INTO users_typing_defaults
		(user_id, lang, write_minutes, learn_minutes, write_config, learn_config, use_focus)
		VALUES ("' . $config->user_id . '","' . $config->lang . '","' . $config->write_minutes . '","' . $config->learn_minutes . '","' . $config->write_config . '","' . $config->learn_config . '","' . $config->use_focus . '" ) ';
		$q .= $updatePart;
		return $this->_db->query ( Database::INSERT, $q, false );
	}

	public function getConfig($userId, $lang) {
		$q = 'SELECT user_id, lang, write_minutes, learn_minutes, write_config, learn_config, use_focus
		    from users_typing_defaults
			where user_id="' . $userId . '"';
		if($lang != null && $lang != "") {
			$q .= ' and lang="' . $lang . '"';
		} else {
			$q .= ' order by updated desc'; // if no lang, then the last one
		}
		$q .= ' limit 1 ';
		
		return $this->_db->query ( Database::SELECT, $q, true )->current ();
	}

	public function getByEmailForPersona($email) {
		$q = 'SELECT id, nick, site_lang, joined, picture, provider, provider_id, TIMESTAMPDIFF(DAY,last_changed_nick,NOW()) as can_change_nick
		    from users
			where
			email ="' . $email . '" and provider="persona"
			limit 1 ';
		return $this->_db->query ( Database::SELECT, $q, true )->current ();
	}

	public function getForLoginWithProvider($id, $provider) {
		$q = 'SELECT  id, nick, site_lang, joined, picture, provider, TIMESTAMPDIFF(DAY,last_changed_nick,NOW()) as can_change_nick 
		    from users u
			where provider_id="' . $id . '" and provider="' . $provider . '" limit 1 ';
		return $this->_db->query ( Database::SELECT, $q, true )->current ();
	}
	
}
