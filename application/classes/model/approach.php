<?php
defined ( 'SYSPATH' ) or die ( 'No direct script access.' );

class Model_Approach extends Kohana_Model {

	public function getAll($modes, $userId) {
		$q = 'select date, wpm, accuracy, keystrokes, typing_minutes, lang, user_id, result 
		from approaches a
		where type in(' . $modes . ') and user_id= "' . $userId . '"
		order by id desc
		limit 100;';
		return $this->_db->query ( Database::SELECT, $q, true )->as_array ();
	}

	public function getSummary($modes, $userId) {
		$q = 'select COUNT(id) as approaches, SUM(typing_minutes) as time_typing, 
				SUM(keystrokes) as keystrokes, round(AVG(accuracy), 2) as avg_accuracy, round(AVG(wpm), 2) as avg_wpm, MAX(wpm) as best_wpm 
		from approaches a
		where type in(' . $modes . ') and user_id= "' . $userId . '";';
		return $this->_db->query ( Database::SELECT, $q, true )->current ();
	}

	public function save($approach) {
		$q = 'INSERT INTO approaches
		(wpm, accuracy, keystrokes, typing_minutes, lang, config, user_id, type, result)
		VALUES ("' . $approach->wpm . '","' . $approach->accuracy . '","' . $approach->keystrokes . '","' . $approach->typingMinutes . '","' . $approach->lang . '","' . $approach->config . '","' . $approach->userId . '","' . $approach->type . '","' . $approach->result . '" ) ';
		return $this->_db->query ( Database::INSERT, $q, false );
	}

	public function saveForCompetition($approachId, $competitionId) {
		$q = 'INSERT INTO competitor_approaches
			(approach_id, competition_id)
			VALUES ("' . $approachId . '","' . $competitionId . '" ) ';
		return $this->_db->query ( Database::INSERT, $q, false );
	}
}
