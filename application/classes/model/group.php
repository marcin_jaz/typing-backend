<?php
defined ( 'SYSPATH' ) or die ( 'No direct script access.' );

class Model_Group extends Kohana_Model {

	public function save($group) {
		$q = 'INSERT INTO groups (lang, name, private_key, created_by)
		VALUES ("' . $group->lang . '","' . $group->groupName . '","' . $group->privateKey . '","' . $group->userId . '" ) ';
		return $this->_db->query ( Database::INSERT, $q, false );
	}

	public function update($group) {
		$q = 'UPDATE groups SET
			show_others=' . $group->show_others;
		
		if($group->is_archived) {
			if($group->archived_time == null) {
				$q .= ', archived_time=CURRENT_TIMESTAMP ';
			}
		} else {
			$q .= ', archived_time=null ';
		}
		
		$q .= ' where id=' . $group->id . ' limit 1;';
		return $this->_db->query ( Database::UPDATE, $q, false );
	}

	public function changeName($group) {
		$q = 'UPDATE groups SET
			name="' . $group->name . '"
			where id=' . $group->id . ' limit 1;';
		return $this->_db->query ( Database::UPDATE, $q, false );
	}

	public function getById($id) {
		$q = 'select * from groups where id=' . $id . ' limit 1;';
		return $this->_db->query ( Database::SELECT, $q, true )->current ();
	}
	
	public function getStudentsWithStats($groupId, $lang, $type) {
		$typeSql = '';
		if($type != 'overall') {
			$typeSql = ' AND a.type="' . $type . '"';
		}
		$q = '
			select u.id, u.nick, u.picture, IFNULL(u.letters,0) as letters, IFNULL(a.approaches,0) as approaches, IFNULL(a.time_typing,0) as time_typing, 
			IFNULL(a.words_typed,0) as words_typed, IFNULL(a.avg_accuracy,0) as avg_accuracy, IFNULL(a.avg_wpm,0) as avg_wpm, IFNULL(a.best_wpm,0) as best_wpm,
			IFNULL(a.last_activity,0) as last_activity, IFNULL(a.best_result,0) as best_result
			from
			(
				select u.id, u.nick, u.picture, IFNULL(utd.learn_config, 0) as letters
				from group_users gu
				join users u on u.id=gu.user_id
				left join users_typing_defaults utd on utd.user_id = u.id and utd.lang="' . $lang . '"
				where gu.group_id= ' . $groupId . '
			) as u 
				left join
			(
				select gu.user_id, COUNT(a.id) as approaches, SUM(a.typing_minutes) as time_typing, round(SUM(a.keystrokes)/5,0) as words_typed, 
				round(AVG(a.accuracy), 2) as avg_accuracy, round(AVG(a.wpm), 2) as avg_wpm, MAX(a.wpm) as best_wpm,
				MAX(a.date) as last_activity, round(MAX(a.result),2) as best_result
				from approaches a 
				join group_users gu on gu.user_id = a.user_id
				left join groups g on g.id=gu.group_id
				where gu.group_id= ' . $groupId . ' and a.lang="' . $lang . '" and a.date>gu.joined ' . $typeSql . ' 
						and IF(g.archived_time is null, 1=1, a.date<g.archived_time)
				group by gu.user_id
			) as a on u.id=a.user_id;';
		
		return $this->_db->query ( Database::SELECT, $q, true )->as_array ();
	}

	public function hasPrivateKey($groupId, $key) {
		$q = 'SELECT id
			  	FROM groups g
				WHERE g.id=' . $groupId . ' AND g.private_key="' . $key . '"
				LIMIT 1';
		$r=$this->_db->query ( Database::SELECT, $q, true )->current ();
		return $this->_db->query ( Database::SELECT, $q, true )->current () != null;
	}

	public function addUser($groupId, $userId) {
		$q = 'INSERT IGNORE INTO group_users (user_id, group_id)
		VALUES ("' . $userId . '","' . $groupId . '" ) ';
		return $this->_db->query ( Database::INSERT, $q, false );
	}

	public function removeUserFromGroup($groupId, $userId) {
		$q = 'DELETE FROM group_users WHERE user_id=' . $userId . ' AND group_id=' . $groupId . ' limit 1';
		return $this->_db->query ( Database::DELETE, $q, false );
	}

	public function getLastGroupIdForUser($userId) {
		$q = 'SELECT id
			  	FROM groups g
				LEFT JOIN group_users gu ON gu.group_id = g.id
				WHERE g.created_by=' . $userId . ' OR gu.user_id=' . $userId . '
				ORDER BY g.id DESC LIMIT 1';
		$v = $this->_db->query ( Database::SELECT, $q, true )->current ();
		if($v == null) {
			return null;
		}
		return $v->id;
	}

	public function getGroupsForUser($userId) {
		$q = 'select id, name, lang, true as is_admin, !ISNULL(archived_time) is_archived 
				from groups
				where created_by=' . $userId . '
				order by id desc;';
		$admin = $this->_db->query ( Database::SELECT, $q, true )->as_array ();
		
		$q = 'select g.id, g.name, lang, false as is_admin, !ISNULL(archived_time) is_archived 
				from group_users gu
				left join groups g on g.id=gu.group_id
				where gu.user_id=' . $userId . '
				order by joined desc;';
		$student = $this->_db->query ( Database::SELECT, $q, true )->as_array ();
		$result = new stdClass ();
		$result->groups_admin = $admin;
		$result->groups_student = $student;
		return $result;
	}
}
