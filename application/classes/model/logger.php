<?php
defined ( 'SYSPATH' ) or die ( 'No direct script access.' );

class Model_Logger extends Kohana_Model {

	public function save($log) {
		// print_r ( $log );
		$db = Database::instance ();
		$q = 'INSERT INTO logs
		(url, message, stacktrace, cause, user)
		VALUES ("' . $log->url . '","' . $db->escape ( $log->message ) . '","' . $db->escape ( $log->stacktrace ) . '","' . $db->escape ( $log->cause ) . '","' . $db->escape ( $log->user ) . '" ) ';
		return $this->_db->query ( Database::INSERT, $q, false );
	}
}
