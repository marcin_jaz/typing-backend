<?php
defined ( 'SYSPATH' ) or die ( 'No direct script access.' );

class Model_Text extends Kohana_Model {

	const WORDS_PER_MINUTE = 80;

	const MAX_WORDS_RESTRICTION = 5000;

	public function getTextForWriting($config) {
		$limit = $config->write_minutes * Model_Text::WORDS_PER_MINUTE;
		return $this->getTextToWrite ( $config->lang, $config->write_config, $limit );
	}

	private function getTextToWrite($lang, $words, $limit) {
		$q = 'select value
		from words_' . $lang;
		if($this->shouldAddPopularRestriction ( $words )) {
			$q .= ' where popular>0 AND popular<=' . $words;
		}
		$q .= ' order by rand() limit 0,' . $limit;
		
		$res = $this->_db->query ( Database::SELECT, $q, true )->as_array ();
		return $this->buildTextFromWords ( $res );
	}

	private function shouldAddPopularRestriction($words) {
		return is_numeric ( $words ) && $words > 0 && $words <= Model_Text::MAX_WORDS_RESTRICTION;
	}
	
	// learn_config is maxGroup
	public function getTextForLearning($config) {
		$limit = ( int ) (22 - $config->learn_config / 2);
		if($config->learn_config == 1) {
			$config->learn_config = 0;
		}
		
		$q = 'select value from words_' . $config->lang;
		$q .= ' where max_group_index = ' . $config->learn_config;
		if($this->shouldAddFocusOnLetter ( $config )) {
			$q .= ' AND letters like "%' . $config->focusLetter . '%"';
		}
		$q .= ' order by rand() limit ' . $limit;
		
		$res = $this->_db->query ( Database::SELECT, $q, true )->as_array ();
		
		return $this->buildTextFromWords ( $res );
	}

	private function buildTextFromWords($res) {
		$text = "";
		foreach($res as &$r) {
			$text .= $r->value . ' ';
		}
		return $text;
	}

	private function shouldAddFocusOnLetter($config) {
		return $config->learn_config > 2 && ! empty ( $config->focusLetter );
	}
}
