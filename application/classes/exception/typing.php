<?php
defined ( 'SYSPATH' ) or die ( 'No direct script access.' );

class Exception_Typing extends Exception {
	var $type;

	function __construct($message, $code = 400, $type = Controller_Typing::ERROR) {
		$this->message = $message;
		$this->code = $code;
		$this->type = $type;
	}

	function getErrorToSend() {
		$error = new stdClass ();
		$error->type = $this->type;
		$error->message = $this->getMessage ();
		$error->shouldTranslate = true;
		return $error;
	}
}