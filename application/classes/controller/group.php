<?php
defined ( 'SYSPATH' ) or die ( 'No direct script access.' );

class Controller_Group extends Controller_Typing {

	public function action_getById($groupId) {
		$m = new Model_Group ();
		$result = $m->getById ( $groupId );
		if($result) {
			$result = $this->encodeAsJson ( $result );
		}
		$this->request->response = $result;
	}

	public function action_getStudentsWithStats() {
		$group = $this->retrieveJsonPost ();
		$m = new Model_Group ();
		$result = $m->getStudentsWithStats ( $group->id, $group->lang, $_GET['type'] );
		if(! $group->show_others && ! $this->isLoggedIn ( $group->created_by )) {
			// others shouldn't be shown only for non admins
			$replacement = '';
			for($i = 1; $i < 100; $i ++) {
				$replacement .= '#';
			}
			foreach($result as $r) {
				$r->nick = substr ( $r->nick, 0, 1 ) . substr ( $replacement, 0, strlen ( $r->nick ) - 1 );
			}
		}
		$this->request->response = $this->encodeAsJson ( $result );
	}

	public function action_save() {
		$group = $this->retrieveJsonPost ();
		$this->checkLoggedInAsUser ( $group->userId );
		
		$m = new Model_Group ();
		$result = $m->save ( $group );
		$groupId = $result[0];
		$this->request->response = $groupId;
	}

	public function action_update() {
		$group = $this->retrieveJsonPost ();
		// only the creator can update
		$this->checkLoggedInAsUser ( $group->created_by );
		
		$m = new Model_Group ();
		$m->update ( $group );
	}

	public function action_changeName() {
		$group = $this->retrieveJsonPost ();
		// only the creator can update
		$this->checkLoggedInAsUser ( $group->created_by );
		
		$m = new Model_Group ();
		$m->changeName ( $group );
	}
	
	// returns 1 if user added, 0 if not
	public function action_addUser() {
		$data = $this->retrieveJsonPost ();
		// only the user himself may join the group
		$this->checkLoggedInAsUser ( $data->userId );
		$m = new Model_Group ();
		if(! $m->hasPrivateKey ( $data->groupId, $data->hash )) {
			throw new Exception_Typing ( 'GROUP_INVALID_KEY', 403 );
		}
		$r = $m->addUser ( $data->groupId, $data->userId );
		$this->request->response = $r[1];
	}

	public function action_removeUser() {
		$data = $this->retrieveJsonPost ();
		// for now only admin can remove the user
		$this->checkLoggedInAsUser ( $data->creatorId );
		$m = new Model_Group ();
		$r = $m->removeUserFromGroup ( $data->groupId, $data->userId );
		$this->request->response = $r[0];
	}

	public function action_getGroupsForUser($userId) {
		$m = new Model_Group ();
		$r = $m->getGroupsForUser ( $userId );
		$this->request->response = $this->encodeAsJson ( $r );
	}

	public function action_getLastGroupIdForUser($userId) {
		$m = new Model_Group ();
		$r = $m->getLastGroupIdForUser ( $userId );
		$this->request->response = $r;
	}
}