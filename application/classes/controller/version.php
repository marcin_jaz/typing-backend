<?php
defined('SYSPATH') or die ('No direct script access.');

class Controller_Version extends Controller_Typing
{

    public function action_get()
    {
        $this->request->response = '1.1.0';
    }
}