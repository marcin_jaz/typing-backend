<?php
defined('SYSPATH') or die ('No direct script access.');

class Controller_Logger extends Controller_Typing
{

    public function action_save()
    {
        $log = $this->retrieveJsonPost();
        $log->stacktrace = implode($log->stacktrace); // because it's array

//        $m = new Model_Logger ();
        // $m->save ( $log );
        if (empty($log->url)) {
            return;
        }
        $body = "Url: " . $log->url . '<br><br>';
        $body .= "Browser: " . $log->browser . '<br><br>';
        $body .= "User:" . $log->user . '<br><br>';
        $body .= "Message: " . $log->message . '<br><br>';
        $body .= "Stacktrace:" . $log->stacktrace . '<br><br>';
        $this->sendEmail("Error: " . $log->url, $body);
    }
}