<?php
defined ( 'SYSPATH' ) or die ( 'No direct script access.' );

class Controller_Approach extends Controller_Typing {

	public function action_all($userId) {
		$modes = $_GET['modes'];
		if(empty ( $modes )) {
			$this->request->response = '[]';
			return;
		}
		
		$m = new Model_Approach ();
		$approaches = $m->getAll ( $modes, $userId );
		$this->request->response = $this->encodeAsJson ( $approaches );
	}

	public function action_summary($userId) {
		$modes = $_GET['modes'];
		if(empty ( $modes )) {
			$this->request->response = '{"approaches":0}';
			return;
		}
		
		$m = new Model_Approach ();
		$approaches = $m->getSummary ( $modes, $userId );
		$this->request->response = $this->encodeAsJson ( $approaches );
	}

	public function action_save() {
		$approach = $this->retrieveJsonPost ();
		// can save only his own approach
		$this->checkLoggedInAsUser ( $approach->userId );
		
		$m = new Model_Approach ();
		$result = $m->save ( $approach );
		$approachId = $result[0];
		$this->request->response = $approachId;
		if($approach->type == 'competition') {
			$m->saveForCompetition ( $approachId, $approach->competition_id );
		}
	}
}