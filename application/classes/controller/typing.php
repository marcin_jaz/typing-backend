<?php
defined('SYSPATH') or die ('No direct script access.');

abstract class Controller_Typing extends Controller
{

    const ERROR = "danger";

    const SECRET_KEY = "calanaprzodkurwa";

    // const WARNING = "warning";
    protected function retrieveJsonPost()
    {
        return json_decode(file_get_contents("php://input"));
    }

    protected function encodeAsJson($v)
    {
        return preg_replace("/\"(\d+(.\d+)?)\"/", '$1', json_encode($v));
        // php version must be upgraded to use that
        // json_encode ( $result, JSON_NUMERIC_CHECK );
    }

    // protected function checkIsSentFromThePage() {
    // $verification = $_GET['v'];
    // // operation inverse to the one done on front
    // $whenSent = $verification * 3 + 999999977;
    // // time() returns the current time measured in the number of seconds since the Unix Epoch
    // $difference = abs ( time () - $whenSent );
    // // if($difference > 30) {
    // // throw new Exception_Typing ( 'YOU', 403 );
    // // }
    // }
    protected function createToken($data)
    {
        /* Create a part of token using secretKey and other stuff */
        $tokenGeneric = Controller_Typing::SECRET_KEY; // .$_SERVER["SERVER_NAME"]; // It can be 'stronger' of course

        /* Encoding token */
        return hash('sha256', $tokenGeneric . $data);
    }

    protected function apache_request_headers()
    {
        $arh = array();
        $rx_http = '/\AHTTP_/';
        foreach ($_SERVER as $key => $val) {
            if (preg_match($rx_http, $key)) {
                $arh_key = preg_replace($rx_http, '', $key);
                $rx_matches = array();
                // do some nasty string manipulations to restore the original letter case
                // this should work in most cases
                $rx_matches = explode('_', $arh_key);
                if (count($rx_matches) > 0 and strlen($arh_key) > 2) {
                    foreach ($rx_matches as $ak_key => $ak_val) $rx_matches[$ak_key] = ucfirst($ak_val);
                    $arh_key = implode('-', $rx_matches);
                }
                $arh[$arh_key] = $val;
            }
        }
        return ($arh);
    }

    protected function isLoggedIn($expectedUserId)
    {
        $headers = $this->apache_request_headers();
        if (isset ($headers['XAUTHORIZATION'])) {
            $auth = $headers['XAUTHORIZATION'];
            if ($auth) {
                list($token, $timestamp, $userId) = explode('-', $auth);
                if ($expectedUserId != null && $expectedUserId != $userId) {
                    return false;
                }
                $userToken = $this->createToken($userId);
                return $userToken == $token;
            }
        }
        return false;
    }

    protected function checkLoggedInAsUser($id)
    {
        // suppose that these can only be sent from the page directly
        // $this->checkIsSentFromThePage ();
        if ($id == 0 || !$this->isLoggedIn($id)) {
            throw new Exception_Typing ('PLEASE_LOGIN', 401);
        }
    }

    protected function checkLoggedIn()
    {
        $this->checkLoggedInAsUser(null);
    }

    protected function sendEmail($to, $title, $body)
    {
//        Ustawienia bezpiecznego połączenia SSL/TLS
//    (Zalecane)
//Nazwa użytkownika:	contact@touchtyping.guru
//Hasło:	Użyj hasła konta poczty e-mail.
//Serwer komunikacji wychodzącej:	az0012.srv.az.pl
//Port SMTP: 465
//Usługi IMAP, POP3 i SMTP wymagają uwierzytelniania.
		//it doesn't work with the headers like below, it used to work though
        //$headers = "MIME-Version: 1.0" . "\r\n";
        //$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        //$headers .= 'From:TouchTyping.guru <contact@touchtyping.guru>' . "\r\n";
		
        return mail($to, $title, $body);
    }
}
