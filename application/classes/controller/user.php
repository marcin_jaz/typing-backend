<?php
defined('SYSPATH') or die ('No direct script access.');

class Controller_User extends Controller_Typing
{

    const DAYS_TO_CHANGE_NICK = 30;

    // for api test
    public function action_ping()
    {
        phpinfo();
        $this->request->response = 'ok';
    }

    // return user as json or false if not found
    public function action_get($id)
    {
        $m = new Model_User ();
        $user = $m->get($id);
        if ($user != null) {
            $this->updateCanChangeNick($user);
            $this->request->response = $this->encodeAsJson($user);
        }
    }

    private function updateCanChangeNick($user)
    {
        $user->can_change_nick = $user->can_change_nick == null || $user->can_change_nick > Controller_User::DAYS_TO_CHANGE_NICK;
    }

    public function action_changeNick($id)
    {
        $this->checkLoggedInAsUser($id);
        $m = new Model_User ();
        $nick = $this->retrieveJsonPost();
        $rowsChanged = $m->changeNick($id, $nick->value);
        if ($rowsChanged == 0) {
            $error = new stdClass ();
            $error->type = Controller_Typing::ERROR;
            $error->message = 'CANT_CHANGE_USERNAME';
            $this->request->response = json_encode($error);
            $this->request->status = 403;
        }
    }

    public function action_changeSiteLang($id)
    {
        $this->checkLoggedInAsUser($id);
        $m = new Model_User ();
        $siteLang = $this->retrieveJsonPost();
        $m->changeSiteLang($id, $siteLang->value);
    }

    public function action_changePicture($id)
    {
        $this->checkLoggedInAsUser($id);

        $image = $this->retrieveJsonPost();
        $filename = uniqid() . $id . $image->name;
        $commonNamePart = 'application/img/avatars/' . $filename;

        $appRoot = parse_url(Kohana::$base_url);
        $targetDir = $_SERVER['DOCUMENT_ROOT'] . $appRoot['path'] . $commonNamePart;
        $targetUrl = Kohana::$base_url . $commonNamePart;
        try {
            $dataEncoded = substr($image->dataUrl, strpos($image->dataUrl, ",") + 1);
            file_put_contents($targetDir, base64_decode($dataEncoded));

            $m = new Model_User ();
            $m->changePicture($id, $targetUrl);
        } catch (Exception $e) {
            $error = new stdClass ();
            $error->type = Controller_Typing::ERROR;
            $error->message = 'CANT_UPLOAD';
            $error->message_params = array(
                'MESSAGE' => $e->getMessage()
            );
            $this->request->response = json_encode($error);
            $this->request->status = 403;
            return;
        }
        $this->request->response = $targetUrl;
    }

    public function action_saveConfig()
    {
        $config = $this->retrieveJsonPost();
        $this->checkLoggedInAsUser($config->user_id);

        $m = new Model_User ();
        $r = $m->saveConfig($config);
        $this->request->response = $r[0];
    }

    private function getDefaultConfigJson($userId, $lang)
    {
        return '{"user_id":"' . $userId . '", "write_minutes":1, "learn_minutes":2, "write_config":1000, "learn_config":3, "use_focus":1, "lang":"' . $lang . '"}';
    }

    public function action_markLangConfigAsLatest($userId)
    {
        $this->checkLoggedInAsUser($userId);
        $config = json_decode($this->getDefaultConfigJson($userId, $_GET['lang']));
        $m = new Model_User ();
        $m->saveDefaultForLangOrMarkLatest($config);
    }

    // gets config for user with given id
    // if no lang given, then the last config is returned
    // if no config at all then default one is returned, without lang, which needs to be set on front the same as site lang
    public function action_getConfig($userId)
    {
        // used also when user not logged in!
        $lang = "";
        if (isset ($_GET['lang'])) {
            $lang = $_GET['lang'];
        }

        $m = new Model_User ();
        // if null return default!
        $result = $m->getConfig($userId, $lang);
        if (empty ($result)) {
            $this->request->response = $this->getDefaultConfigJson($userId, $lang);
        } else {
            $this->request->response = $this->encodeAsJson($result);
        }
    }

    /**
     * Gets below parameters and inserts the user if not present, otherwise just logs in.
     * provider_id
     * email
     */
    public function action_loginByEmail()
    {
        $user = $this->retrieveJsonPost();
        $m = new Model_User ();
        $existingUser = $m->getByEmailForPersona($user->email);
        if ($existingUser == null) {
            // ajust missing properties
            $defaultPicture = 'application/img/avatars/default.jpg';
            $user->picture = Kohana::$base_url . $defaultPicture;
            $array = explode("@", $user->email); // use variable otherwise strict error
            $user->name = array_shift($array);
            $user->id = $user->provider_id; // provider id in fact is saved from id field
            $user->gender = 'unknown';

            $m->insert($user);
            //fetch again to get db filled fields
            $existingUser = $m->getByEmailForPersona($user->email);
            $existingUser->token = $this->createToken($existingUser->id);
            $this->request->response = $this->encodeAsJson($existingUser);
        } else {
            if ($existingUser->provider_id == $user->provider_id) {
                //if the code is equal it means the user can be logged in
                $existingUser->token = $this->createToken($existingUser->id);
                $this->request->response = $this->encodeAsJson($existingUser);
            } else {
                $error = new stdClass ();
                $error->type = Controller_Typing::ERROR;
                $error->message = 'CANT_LOGIN';
                $this->request->response = $this->encodeAsJson($error);
                $this->request->status = 404;
            }
        }

    }

    public function action_login()
    {
        $m = new Model_User ();
        $user = $this->retrieveJsonPost();

        if ($user->provider == 'persona') {
            //only email sent here, no actual login
            if (!empty ($user->email)) {
                $existingUser = $m->getByEmailForPersona($user->email);
                if ($existingUser != null) {
                    if ($existingUser->provider_id == null) {
                        $user->provider_id = $this->createEmailProviderId();
                        $m->changeProviderId($existingUser->id, $user->provider_id);
                    }else{
                        $user->provider_id=$existingUser->provider_id;
                    }
                } else {
                    $user->provider_id = $this->createEmailProviderId();
                }
                $body = $user->base_url . '?code=' . $user->provider_id . '&email=' . $user->email;
                $isAcceptedForDelivery = $this->sendEmail($user->email, "TouchTyping.guru LOGIN", $body);
				echo $isAcceptedForDelivery;
                return;
            }
        } else {
            //login with given provider
            $loggedUser = $m->getForLoginWithProvider($user->id, $user->provider);
            if ($loggedUser == null) {
                // $wasInserted = true;
                $m->insert($user);
                $loggedUser = $m->getForLoginWithProvider($user->id, $user->provider);
            }
            if ($loggedUser) {
                $loggedUser->token = $this->createToken($loggedUser->id);

                $this->updateCanChangeNick($loggedUser);
                $this->request->response = $this->encodeAsJson($loggedUser);
                return;
            }
        }
        $error = new stdClass ();
        $error->type = Controller_Typing::ERROR;
        $error->message = 'CANT_LOGIN';
        $this->request->response = $this->encodeAsJson($error);
        $this->request->status = 404;
    }

    private function createEmailProviderId()
    {
        $uuid = uniqid('', true);
        return substr($uuid, 0, 40);
    }

}