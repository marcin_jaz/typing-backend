<?php
defined('SYSPATH') or die ('No direct script access.');

class Controller_Competition extends Controller_Typing
{

    const MINUTES_TO_CREATE_NEW = 60;//only one competition per hour may be created

    public function action_all()
    {
        $m = new Model_Competition ();
        // sleep(3);
        $competitions = $m->getAll();
        $this->request->response = $this->encodeAsJson($competitions);
    }

    public function action_save()
    {
        $competition = $this->retrieveJsonPost();
        // only founder can save the competition
        $this->checkLoggedInAsUser($competition->founder_user_id);

        $m = new Model_Competition ();
        $lastCreated = $m->getCompetitionLastCreatedByUser($competition->founder_user_id);
        if ($lastCreated != null && $lastCreated->minutes_since_created < Controller_Competition::MINUTES_TO_CREATE_NEW) {
            $createdAgo = $lastCreated->minutes_since_created;
            $this->request->status = 403;
            $error = new stdClass ();
            $error->message = 'COMPETITION_LAST_CREATED';
            $error->message_params = array(
                'AGO' => $createdAgo,
                'LAST_ID' => $lastCreated->id,
                'LAST_KEY' => $lastCreated->private_key
            );
            if ($competition->group_id == "NULL") {
                //should stay on group page in case it was violated in the group
                $error->href = 'competitions';
            }
            $error->type = Controller_Typing::ERROR;
            $this->request->response = $this->encodeAsJson($error);
        } else {
            $result = $m->save($competition);
            $this->request->response = $result[0]; // id
        }
    }

    public function action_get($id)
    {
        $m = new Model_Competition ();
        $result = $m->get($id);
        $this->request->response = $this->encodeAsJson($result);
    }

    public function action_getIfActive($params)
    {
        list($id, $key) = explode('/', $params);
        $m = new Model_Competition ();
        $result = $m->getIfActive($id, $key);
        if ($result != null) {
            $this->request->response = json_encode($result);
        } else {
            $this->request->status = 403;
            $error = new stdClass ();
            $error->message = 'COMPETITION_NOT_AVAILABLE';
            $error->href = 'competitions';
            $error->type = Controller_Typing::ERROR;
            $this->request->response = json_encode($error);
        }
    }

    public function action_getPositionsOfCompetition($id)
    {
        $m = new Model_Competition ();
        $result = $m->getPositionsOfCompetition($id);
        $this->request->response = $this->encodeAsJson($result);
    }

    public function action_getPositionsOfCompetitionsInGroup($groupId)
    {
        $m = new Model_Competition ();
        $competitions = $m->getCompetitionsForGroup($groupId);
        $ids = array();
        foreach ($competitions as $c) {
            $ids[] = $c->id;
        }
        $result = new StdClass ();
        $result->competitions = $competitions;
        $result->positions = $m->getPositionsOfCompetitionsForGroup($groupId, $ids);
        $this->request->response = $this->encodeAsJson($result);
    }

    public function action_getUserHistory($userId)
    {
        $m = new Model_Competition ();
        $result = $m->getUserHistory($userId);
        $this->request->response = $this->encodeAsJson($result);
    }

    public function action_getUserPositionsHistory($userId)
    {
        $m = new Model_Competition ();
        $result = $m->getUserHistoryFirstThreePositions($userId);
        $this->request->response = $this->encodeAsJson($result);
    }
}